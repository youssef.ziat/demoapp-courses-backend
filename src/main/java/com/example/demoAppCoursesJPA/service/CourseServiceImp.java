package com.example.demoAppCoursesJPA.service;

import com.example.demoAppCoursesJPA.entity.Course;
import com.example.demoAppCoursesJPA.repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CourseServiceImp implements CourseService {

    @Autowired
    CourseRepository courseRepository ;


    @Override
    public List<Course> findAll() {
        return courseRepository.findAll() ;
    }

    @Override
    public void AddCourse(Course course) {
        courseRepository.save(course) ;
    }

    @Override
    public Optional<Course> findCourseById(String id) {
        return courseRepository.findById(id);
    }

    @Override
    public void updateCourse(Course course) {
            courseRepository.save(course) ;
    }

    @Override
    public void deleteCourse(String courseId) {
            courseRepository.deleteById(courseId);
    }
}
