package com.example.demoAppCoursesJPA.service;

import com.example.demoAppCoursesJPA.entity.Course;

import java.util.List;
import java.util.Optional;

public interface CourseService {
    public List<Course> findAll();
    public void AddCourse(Course course);
    public Optional<Course> findCourseById(String id);
    public void updateCourse(Course course);
    public void deleteCourse(String courseId);
}
