package com.example.demoAppCoursesJPA.entity;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Course {
    @Id
    String courseId;
    @NotNull
    String courseName;
    String courseAuthor;
    String courseAuthorMail;
    @NotNull
    String courseImageUrl;
}
