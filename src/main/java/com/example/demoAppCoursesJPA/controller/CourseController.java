package com.example.demoAppCoursesJPA.controller;


import com.example.demoAppCoursesJPA.entity.Course;
import com.example.demoAppCoursesJPA.service.CourseService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

@CrossOrigin("*")
@RestController
@RequestMapping("/courses")
public class CourseController {

    @Resource
    CourseService courseService;
    @GetMapping("/")
    public @ResponseBody
    String helloWorld(){
        return "Hello World" ;
    }
    @GetMapping("/all")
    public List<Course> findAll(){
        return courseService.findAll() ;
    }
    @GetMapping("/course")
    public Optional<Course> findCourseById(@RequestParam String id){
        return courseService.findCourseById(id) ;
    }
    @PostMapping("/addCourse")
    public ResponseEntity<Void> addCourse(@RequestBody Course course){
        courseService.AddCourse(course);
        if( course != null ) {
            return ResponseEntity.noContent().build() ;
        }
        else{
            ResponseEntity.status(404) ;
            return ResponseEntity.notFound().build();

        }
    }
    @PostMapping("/updateCourse")
    public ResponseEntity<Void> updateCourse(@RequestBody Course course){
        System.out.println(course);
        courseService.updateCourse(course) ;
        if(course!=null){
            return ResponseEntity.noContent().build() ;
        }
        else{
            return ResponseEntity.notFound().build() ;
        }
    }
    @DeleteMapping("/deleteCourse")
    public ResponseEntity<Void> deleteCourse(@RequestParam String courseId){

        courseService.deleteCourse(courseId);
        if(courseId != null ){
            return ResponseEntity.noContent().build();
        }
        else{
            return ResponseEntity.notFound().build();
        }
    }
}
