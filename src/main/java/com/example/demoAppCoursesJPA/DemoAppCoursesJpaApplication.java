package com.example.demoAppCoursesJPA;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoAppCoursesJpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoAppCoursesJpaApplication.class, args);
	}

}
